# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GkoPlottingTool
                                 A QGIS plugin
 Plotting GKO data
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2019-12-17
        git sha              : $Format:%H$
        copyright            : (C) 2019 by Simon Makwarth, Miløstyrelsen
        email                : simak@mst.dk
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .gko_plotting_tool_dialog import GkoPlottingToolDialog
import os.path


############ NEW IMPORTS#####
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QApplication, QLabel
from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction
from qgis.PyQt.QtCore import pyqtSignal, Qt

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
# from .script_launcher_dialog import ScriptLauncherDialog
from .script_maptool import ScriptMaptool
import os.path
import configparser




from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication, Qt
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction
# Initialize Qt resources from file resources.py
from .resources import *

# Import the code for the DockWidget
from .gko_plotting_tool_dockwidget import GkoPlotingToolDockWidget
import os.path
from .script_maptool import ScriptMaptool
import os.path
import configparser

class GkoPlottingTool:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'GkoPlottingTool_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&GKO plotting tool')

        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        self.first_start = None

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('GkoPlottingTool', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/gko_plotting_tool/icons/info.png'
        self.add_action(
            icon_path,
            text=self.tr(u'GKO plotter Help'),
            callback=self.run_help,
            parent=self.iface.mainWindow())

        # will be set False in run()
        self.first_start = True

        icon_path = ':/plugins/gko_plotting_tool/icons/skytem.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Plot SkyTEM'),
            callback=self.run_skytem_plotter,
            parent=self.iface.mainWindow())

        icon_path = ':/plugins/gko_plotting_tool/icons/tem.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Plot TEM'),
            callback=self.run_tem_plotter,
            parent=self.iface.mainWindow())

        icon_path = ':/plugins/gko_plotting_tool/icons/waterlevel.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Plot Waterlevel'),
            callback=self.run_waterlevel_plotter,
            parent=self.iface.mainWindow())

        icon_path = ':/plugins/gko_plotting_tool/icons/killall.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Close all plots'),
            callback=self.run_killall,
            parent=self.iface.mainWindow())

    def getVersion(self):
        config = configparser.ConfigParser()
        config.read(os.path.join(os.path.dirname(__file__), 'metadata.txt'))

        version = str(config.get('general', 'version'))
        return version

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&GKO plotting tool'),
                action)
            self.iface.removeToolBarIcon(action)

    def run_help(self):
        if self.first_start == True:
            self.first_start = False
            self.dlg = GkoPlottingToolDialog()

            label1 = QLabel(self.dlg)
            label1.setMargin(20)
            version = self.getVersion()

            html = f"""
            <span>
                <h3>GKO plotting tool {version}</h3><br>
                Dette plugin plotter GKO data.<br>
                Ved at trykke på en af knapperne, <br>
                kan en firkant trækkes <br>
                Alt efter hvilken knap der vælges,<br>
                plottes data indenfor firkanten.
                <br><br>

            </span>
            """
            label1.setText(html)

        # show the dialog
        self.dlg.setFixedSize(400, 300)
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            self.dlg.close()

    def run_skytem_plotter(self):
        msg = 'skytem'

        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_tem_plotter(self):
        msg = 'tem'

        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_waterlevel_plotter(self):
        msg = 'waterlevel'

        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_killall(self):
        msg = 'kill_all'

        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)
