#!/usr/bin/env python3
import os
import sys
"""
Plotting sounding curve and inverted geophysical model from SkyTEM (AEM), TEM, and MEP (ERT) data.
"""

__author__ = "Simon Makwarth <simak@mst.dk>"
__version__ = "0.1.0"
__license__ = "GNUv3"


class PlotData:
    def __init__(self, xmin_val, ymin_val, xmax_val, ymax_val, datatype_val):
        """
        :param xmin_val: geometry extent xy-plane, minimum x-value
        :param ymin_val: geometry extent xy-plane, minimum y-value
        :param xmax_val: geometry extent xy-plane, maximum x-value
        :param ymax_val: geometry extent xy-plane, maximum x-value
        """
        self.pghost = "10.33.131.50"
        self.pgport = "5432"
        self.pgdatabase = 'pcgerda'
        self.pgdbjupiter = 'pcjupiterxl'

        self.pguser = os.environ.get('DB_READER_USER')
        self.pgpassword = os.environ.get('DB_READER_PASS')
        # self.pguser = os.environ.get('DB_ADMIN_USER')
        # self.pgpassword = os.environ.get('DB_ADMIN_PASS')
        # self.pguser = os.environ.get('grukosadmin')
        # self.pgpassword = os.environ.get('merkel30rblaser')
        self.xmin = xmin_val
        self.ymin = ymin_val
        self.xmax = xmax_val
        self.ymax = ymax_val
        self.datatype_val = datatype_val

    def import_db_to_df(self, sql):
        """
        Imports sql query to pandas dataframe
        :param sql: sql string
        :return: pandas dataframe from sql query
        """
        import pandas as pd
        import psycopg2 as pg
        if self.datatype_val == 'waterlevel':
            pgdatabase = self.pgdbjupiter
        else:
            pgdatabase = self.pgdatabase
        try:
            con = pg.connect(
                host=self.pghost,
                port=self.pgport,
                database=pgdatabase,
                user=self.pguser,
                password=self.pgpassword
            )
        except Exception as e:
            print('Unable to connect to database: ' + self.pgdatabase)
            print(e)
        else:
            try:
                imported_df = pd.read_sql(sql, con)
                return imported_df
            except Exception as e:
                print('sql could not be executed')
                print(e)
                return None

    def execute_sql(self, sql):
        """
        Execute sql to database connection either from sql or sql-file
        :return: column_names and query-result
        :param sql: path to .sql file or sql string
        """
        import psycopg2 as pg
        try:
            con = pg.connect(
                host=self.pghost,
                port=self.pgport,
                database=self.pgdatabase,
                user=self.pguser,
                password=self.pgpassword
            )
        except Exception as e:
            print('Unable to connect to database: ' + self.pgdatabase)
            print(e)
        else:
            try:
                cur = con.cursor()
                cur.execute(sql)
                results = cur.fetchall()
                desc = cur.description
                column_names = [col[0] for col in desc]
            except:
                print('Unable to execute sql: {}'.format(sql))
            else:
                return column_names, results
                cur.close()
            con.close()

    def tem_data(self):
        """
        Plots TEM sounding and model using matplotlib
        :rtype: object
        """
        import matplotlib.pyplot as plt  # pip install matplotlib

        def get_tem_data():
            """
            sql execution fetching tem sounding data from database
            :return: list from sql query containing tem soundings
            """
            tem_data_sql = f"""                WITH 
                    temdata AS (
                        SELECT 
                            ofr.dataset, ofr.daposition, ofr.segment, ofr."sequence", 
                            ofr.abscival, ofr.ordiresval, ofr.ordimeaval, ofr.ordistdval
                        FROM pcgerda.odvfwres ofr
                        INNER JOIN pcgerda.temhea th ON ofr.dataset = th.dataset
                        INNER JOIN pcgerda.dataset dat ON ofr.dataset = dat.dataset
                        INNER JOIN pcgerda.geometry geo ON ofr.model = geo.model
                        WHERE upper(th.temsubtype) IN ('HMTEM1', 'HMTEM2', 'PATEM', 'TEM40', 'TEMCL', 'TEMOFF', 'TEMCO')
                            AND geom && ST_MakeEnvelope({self.xmin}, {self.ymin}, {self.xmax}, {self.ymax}, 25832)
                    )
                SELECT * FROM temdata
                ORDER BY dataset, daposition, "sequence";
            """
            column_names, tem_data = self.execute_sql(tem_data_sql)
            return column_names, tem_data

        def get_tem_model():
            """
            sql execution fetching tem models from database
            :return: list from sql query containing tem models
            """
            tem_model_sql = f"""
                WITH 
                    temmodel AS (
                        SELECT DISTINCT
                            ofr.model, ofr.moposition, ol.layer, ol.rho, 
                            ol.rhostddev, ol.thickness, ol.thkstddev, ol.depbottom
                        FROM pcgerda.odvfwres ofr
                        INNER JOIN pcgerda.temhea th ON ofr.dataset = th.dataset
                        INNER JOIN pcgerda.dataset dat ON ofr.dataset = dat.dataset
                        INNER JOIN pcgerda.geometry geo ON ofr.model = geo.model
                        INNER JOIN pcgerda.odvlayer ol ON ofr.model = ol.model AND ofr.moposition = ol."position"
                        WHERE upper(th.temsubtype) IN ('HMTEM1', 'HMTEM2', 'PATEM', 'TEM40', 'TEMCL', 'TEMOFF', 'TEMCO')
                            AND geom && ST_MakeEnvelope({self.xmin}, {self.ymin}, {self.xmax}, {self.ymax}, 25832)
                    )
                SELECT * FROM temmodel
                ORDER BY model, moposition, layer
            """
            column_names, tem_model = self.execute_sql(tem_model_sql)
            return column_names, tem_model

        def plot_tem(data_tem, model_tem):
            """
            plotting function for tem data
            :param data_tem: list from sql query containing tem soundings
            :param model_tem: list from sql query containing tem models
            """
            # setup figure
            figures = []
            unique_datasets = []
            for row in data_tem:
                if row[0] not in unique_datasets:
                    unique_datasets.append(row[0])

            unique_models = []
            for row in model_tem:
                if row[0] not in unique_models:
                    unique_models.append(row[0])
            k = 0
            for unique_dataset, unique_model in zip(unique_datasets, unique_models):
                # initiate plot
                fig, (ax1, ax2) = plt.subplots(1, 2)
                ax1.set_title(f'TEM dataset: {unique_dataset}')
                ax2.set_title(f'TEM model: {unique_model}')

                # plot tem data
                abscival = []
                ordimeaval = []
                ordistdval = []
                ordiresval = []
                color = []
                for row in data_tem:
                    if row[0] == unique_dataset:
                        # fetch data
                        abscival.append(row[4])
                        ordiresval.append(row[5])
                        ordimeaval.append(row[6])
                        ordistdval.append(row[7])

                        # add color scheme
                        if row[2] == 1:
                            color.append('blue')
                        elif row[2] == 2:
                            color.append('orange')
                        else:
                            color.append('green')
                low_moment = []
                mid_moment = []
                high_moment = []
                low_time = []
                mid_time = []
                high_time = []
                for i, col in enumerate(color):
                    if col == 'blue':
                        low_moment.append(ordiresval[i])
                        low_time.append(abscival[i])
                    if col == 'orange':
                        mid_moment.append(ordiresval[i])
                        mid_time.append(abscival[i])
                    if col == 'green':
                        high_moment.append(ordiresval[i])
                        high_time.append(abscival[i])
                ax1.plot(low_time, low_moment, color='black')
                ax1.plot(mid_time, mid_moment, color='black')
                ax1.plot(high_time, high_moment, color='black')

                for i, col in enumerate(color):
                    # calculate errorbar from std-factor
                    try:
                        error = ((ordimeaval[i]) * (ordistdval[i])) - ((ordimeaval[i]) / (ordistdval[i]))
                    except:
                        error = None

                    # plot datapoints and errorbars if defined
                    if error is not None:
                        ax1.errorbar(abscival[i], ordimeaval[i], error,
                                     fmt='.', color=color[i], ecolor=color[i])
                    else:
                        ax1.scatter(abscival[i], ordimeaval[i], color=color[i])
                    ax1.set_yscale('log')
                    ax1.set_xscale('log')
                    ax1.set(xlabel='Time [s]', ylabel='dB/dt')

                # plot tem models
                depth_points = []
                res_points = []
                for i, row in enumerate(model_tem):
                    if row[0] == unique_model:
                        # plot model at z=0 if first layer
                        if row[2] == 1:
                            # plot first layer
                            depth_points.append(0)
                            res_points.append(row[3])

                        if row[7] is None:
                            # plot last layer
                            res_points.append(row[3])
                            depth_points.append(model_tem[i - 1][7] + 20)

                        else:
                            # plot layer
                            depth_points.append(row[7])
                            res_points.append(row[3])

                            # connect layer to next layer
                            depth_points.append(row[7])
                            res_points.append(model_tem[i + 1][3])

                ax2.plot(res_points, depth_points, color='red')
                ax2.invert_yaxis()
                ax2.set(xlabel='Resistivity [Ω]', ylabel='Depth [m]')
                ax1.grid(True)
                ax2.grid(True)
                plt.tight_layout()
                fig.canvas.set_window_title(f'TEM Dataset {unique_dataset} Model {unique_model}')
                figures.append(plt)
                plt.show()
            return figures

        dataset_column, tem_data = get_tem_data()
        model_column, tem_model = get_tem_model()
        tem_figures = plot_tem(tem_data, tem_model)
        return tem_figures
        
    def skytem_data(self):
        """
        Plots Skytem sounding and model using matplotlib
        :rtype: object
        """
        import matplotlib.pyplot as plt  # pip install matplotlib
        from decimal import Decimal

        def get_skytem_data():
            """
            sql execution fetching tem sounding data from database
            :return: list from sql query containing skytem soundings
            """
            skytem_data_sql = f"""
                WITH skytemdata AS (
                    SELECT 
                        ofr.model, 
                        ofr.dataset, 
                        ofr.daposition, 
                        ofr.segment, 
                        ofr."sequence", 
                        ofr.abscival, 
                        ofr.ordiresval, 
                        ofr.ordimeaval, 
                        ofr.ordistdval, 
                        ofr.moposition
                    FROM pcgerda.geometry geo
                    LEFT JOIN pcgerda.odvfwres ofr ON geo.model = ofr.model AND geo."position" = ofr.moposition
                    LEFT JOIN pcgerda.SKYHEA sh ON ofr.DATASET = sh.DATASET
                    LEFT JOIN pcgerda.MODEL m ON ofr.model = m.model
                    WHERE lower(COALESCE(sh.temsubtype, m.settings)) ilike '%skytem%'
                        AND geom && ST_MakeEnvelope({self.xmin}, {self.ymin}, {self.xmax}, {self.ymax}, 25832)
                    )
                SELECT * FROM skytemdata
                ORDER BY model, moposition, daposition, segment, "sequence";
            """
            column_names, skytem_data = self.execute_sql(skytem_data_sql)
            return column_names, skytem_data

        def get_skytem_model():
            """
            sql execution fetching skytem models from database
            :return: list from sql query containing skytem models
            """
            skytem_model_sql = f"""                
                WITH 
                    skytemmodel AS (              
                        SELECT DISTINCT 
                            ol.model, 
                            ol."position" as moposition, 
                            ol.layer, 
                            ol.rho, 
                            ol.rhostddev, 
                            ol.thickness, 
                            ol.thkstddev, 
                            ol.depbottom,
                            ol.dbotstddev,
                            ol.rho/ol.rhostddev AS rho_min, 
                            ol.rho*ol.rhostddev AS rho_max, 
                            ol.depbottom/ol.dbotstddev AS dep_min, 
                            ol.depbottom*ol.dbotstddev AS dep_max,
                            od.doiupper,
                            od.doilower
                        FROM pcgerda.geometry geo 
                        LEFT JOIN pcgerda.odvlayer ol ON geo.model = ol.model AND geo."position" = ol."position"
                        LEFT JOIN pcgerda.model m ON geo.model = m.model
                        LEFT JOIN pcgerda.odvfwres ofr ON geo.model = ofr.model AND geo."position" = ofr.moposition
                        LEFT JOIN pcgerda.SKYHEA sh ON ofr.dataset = sh.DATASET
                        LEFT JOIN pcgerda.odvdoi od ON ofr.model = od.model AND ofr.moposition = od."position" 
                        WHERE lower(COALESCE(sh.temsubtype, m.settings)) ilike '%skytem%'
                            AND geom && ST_MakeEnvelope({self.xmin}, {self.ymin}, {self.xmax}, {self.ymax}, 25832)
                        )
                SELECT * FROM skytemmodel
                ORDER BY model, moposition, layer;
            """
            column_names, skytem_model = self.execute_sql(skytem_model_sql)
            return column_names, skytem_model

        def plot_skytem(data_skytem, model_skytem):
            """
            plotting function for skytem data
            :return: list of figures
            :param data_skytem: list from sql query containing skytem soundings
            :param model_skytem: list from sql query containing skytem models
            """

            def plot_skytem_data(value_point, time_point, stdf_point, color_point):
                try:
                    error = (value_point * stdf_point) - (value_point / stdf_point)
                except:
                    ax1.scatter(time_point, value_point, color=color_point)
                else:
                    ax1.errorbar(time_point, value_point, error,
                                 fmt='.', color=color_point, ecolor=color_point)

            figures = []
            unique_model_positions = []
            for row in model_skytem:
                if row[1] not in unique_model_positions:
                    unique_model_positions.append(row[1])

            unique_models = []
            for row in model_skytem:
                if row[0] not in unique_models:
                    unique_models.append(row[0])

            for unique_model_position in unique_model_positions:
                for unique_model in unique_models:

                    # initiate plot
                    fig, (ax1, ax2) = plt.subplots(1, 2)

                    ### plot tem data ###
                    # forward response from model
                    low_moment = []
                    mid_moment = []
                    high_moment = []
                    low_time = []
                    mid_time = []
                    high_time = []

                    # plot data
                    for i, row in enumerate(data_skytem):
                        if row[9] == unique_model_position and row[0] == unique_model:
                            # add color scheme
                            if row[3] == 1:
                                color = 'blue'
                                plot_skytem_data(row[7], row[5], row[8], color)
                                low_moment.append(row[6])
                                low_time.append(row[5])
                            elif row[3] == 2:
                                color = 'orange'
                                plot_skytem_data(row[7], row[5], row[8], color)
                                mid_moment.append(row[6])
                                mid_time.append(row[5])
                            else:
                                color = 'green'
                                plot_skytem_data(row[7], row[5], row[8], color)
                                high_moment.append(row[6])
                                high_time.append(row[5])

                    ax1.plot(low_time, low_moment, color='black')
                    ax1.plot(mid_time, mid_moment, color='black')
                    ax1.plot(high_time, high_moment, color='black')

                    ### plot tem models ###
                    depth_points = []
                    res_points = []
                    depth_min = []
                    depth_max = []
                    res_min = []
                    res_max = []
                    doi_lower_list = []
                    doi_upper_list = []
                    for i, row in enumerate(model_skytem):
                        if row[1] == unique_model_position and row[0] == unique_model:
                            # plot model at z=0 if first layer
                            doi_lower_list.append(row[14])
                            doi_upper_list.append(row[13])
                            if row[2] == 1:
                                # plot first layer
                                depth_points.append(0)
                                res_points.append(row[3])
                                depth_min.append(0)
                                res_min.append(row[8])
                                depth_max.append(0)
                                res_max.append(row[9])

                            if row[7] is None:
                                # plot last layer
                                res_points.append(row[3])
                                depth_points.append(model_skytem[i - 1][7] + 20)
                                depth_min.append(model_skytem[i - 1][7] + 20)
                                depth_max.append(model_skytem[i - 1][7] + 20)
                                res_min.append(row[8])
                                res_max.append(row[9])
                            else:
                                # plot layer
                                depth_points.append(row[7])
                                res_points.append(row[3])
                                depth_min.append(row[10])
                                res_min.append(row[8])
                                depth_max.append(row[11])
                                res_max.append(row[9])

                                # connect layer to next layer
                                depth_points.append(row[7])
                                res_points.append(model_skytem[i + 1][3])
                                depth_min.append(row[10])
                                res_min.append(model_skytem[i + 1][8])
                                depth_max.append(row[11])
                                res_max.append(model_skytem[i + 1][9])

                    # fetch doi
                    try:
                        doi_upper = [max(doi_upper_list)] * 2
                        doi_lower = [max(doi_lower_list)] * 2
                        factor = (max(res_points)-min(res_points))*Decimal('0.1')
                        range_doi = [min(res_points)-factor, max(res_points)+factor]
                    except:
                        pass
                    else:
                        ax2.plot(range_doi, doi_upper, '--', color='darkgrey', label='DOI upper')
                        ax2.plot(range_doi, doi_lower, '--', color='black', label='DOI lower')

                    # plot
                    ax2.plot(res_points, depth_points, color='red', label='Model')
                    # ax2.plot(res_min, depth_min, '--', color='blue')
                    # --ax2.scatter(res_max, depth_max, '--o', color='red')

                    # setup labels and axis
                    if self.datatype_val in ['tem', 'skytem']:
                        y_label = 'dB/dt'
                        x_label = 'Time [s]'
                        ax1.set_yscale('log')
                        ax1.set_xscale('log')
                        if self.datatype_val in ['skytem']:
                            fig.canvas.set_window_title(
                                f'{self.datatype_val.capitalize()} '
                                f'Model{unique_model} ModelPosition{unique_model_position}')
                    elif self.datatype_val in ['mep']:
                        y_label = 'rhoa'
                        x_label = 'Noget andet'

                    ax1.set_title(f'{self.datatype_val.capitalize()} sounding')
                    ax1.set(xlabel=x_label, ylabel=y_label)
                    ax1.grid(True)
                    ax2.set_title(f'{self.datatype_val.capitalize()} model')
                    ax2.invert_yaxis()
                    ax2.set(xlabel='Resistivity [Ωm]', ylabel='Depth [m]')
                    plt.tight_layout()
                    ax2.grid(True)
                    # ax2.legend()#loc='upper center', bbox_to_anchor=(0.5, -0.1), fancybox=True, shadow=True, ncol=3)
                    figures.append(plt)
            return figures

        dataset_column, skytem_data = get_skytem_data()
        model_column, skytem_model = get_skytem_model()
        skytem_figures = plot_skytem(skytem_data, skytem_model)
        return skytem_figures

    def mep_data(self):
        """
        Plots mep sounding and model using matplotlib
        :rtype: object
        """
        def get_mep_data():
            """
            sql execution fetching mep sounding data from database
            :return: list from sql query containing mep soundings (apparent resistivity)
            """
            pass

        def get_mep_model():
            """
            sql execution fetching mep models from database
            :return: list from sql query containing mep models
            """
            pass

        def plot_mep():
            """
            plotting function for MEP data
            :param data_mep: list from sql query containing mep soundings
            :param model_mep: list from sql query containing mep models
            """
            pass

    def waterlevel_data(self):
        import pandas as pd  # pip install pandas
        import matplotlib.pyplot as plt  # pip install matplotlib
        from pandas.plotting import register_matplotlib_converters
        register_matplotlib_converters()

        def get_waterlevel_data():
            wl_data_sql = f"""
                SELECT 
                    wl.boreholeno, 
                    wl.intakeno, 
                    scr.screenno,
                    wl.timeofmeas, 
                    wl.watlevmsl
                FROM jupiter.watlevel wl
                LEFT JOIN jupiter.screen scr ON wl.boreholeid = scr.boreholeid 
                    AND wl.intakeno = scr.intakeno AND wl.intakeid = scr.intakeid
                LEFT JOIN jupiter.borehole b ON wl.boreholeid = b.boreholeid
                WHERE wl.timeofmeas IS NOT NULL 
                    AND watlevmsl IS NOT NULL
                    AND b.geom && ST_MakeEnvelope({self.xmin}, {self.ymin}, {self.xmax}, {self.ymax}, 25832)
            """
            df = self.import_db_to_df(wl_data_sql)
            return df

        def plot_wl_data():
            print('### Initiating sql..')
            df = get_waterlevel_data()
            print('### Dataframe created from sql')
            figures = []
            df['timeofmeas'] = pd.to_datetime(df['timeofmeas'])
            df['watlevmsl'] = pd.to_numeric(df['watlevmsl'])
            df.set_index('timeofmeas', inplace=True)

            df_grp = df.groupby(['boreholeno'])
            for name, grp in df_grp:
                fig = plt.figure()
                fig.canvas.set_window_title(f'Boreholeno {name}')
                grp.groupby(['intakeno'])['watlevmsl'].plot(style='.-')
                plt.legend(title="Intake", frameon=False, loc='lower center', ncol=2)
                plt.xlabel('Time')
                plt.ylabel('Waterlevel elevation [m]')
                plt.title(f'Boreholeno:{name}')
                plt.grid(True)
                plt.show()
                figures.append(plt)
            return figures

        figures = plot_wl_data()
        print('### Figures are all store in a variable')
        return figures


def kill_all():
    pass


def plot_data(datatype_val, xmin_val, ymin_val, xmax_val, ymax_val):
    """
    Initiate function for class PlotData
    :param datatype_val: string containing the datatype to be plotted (eg. tem, skytem, mep)
    :param xmin_val: min bound for x in xy-plane
    :param ymin_val: min bound for y in xy-plane
    :param xmax_val: max bound for x in xy-plane
    :param ymax_val: max bound for y in xy-plane
    """
    datatype_val = datatype_val.lower()
    print(f'{datatype_val} {xmin_val} {ymin_val} {xmax_val} {ymax_val}')
    plot_dat = PlotData(xmin_val, ymin_val, xmax_val, ymax_val, datatype_val)
    if datatype_val == 'tem':
        figures = plot_dat.tem_data()
        return figures
    elif datatype_val == 'skytem':
        figures = plot_dat.skytem_data()
        return figures
    elif datatype_val == 'mep':
        figures = plot_dat.mep_data()
        return figures
    elif datatype_val == 'waterlevel':
        figures = plot_dat.waterlevel_data()
        return figures
    elif datatype_val == 'kill_all':
        kill_all()
    else:
        pass
