# -*- coding: utf-8 -*-
from __future__ import absolute_import

import configparser
import os
import subprocess
import sys

from PyQt5.QtWidgets import QMessageBox
from qgis.PyQt.QtCore import pyqtSignal, Qt
from qgis.PyQt.QtGui import QColor, QCursor
from qgis.PyQt.QtWidgets import QApplication, QTreeWidgetItemIterator

from qgis.PyQt import QtGui, uic, QtCore

import qgis
from qgis.core import QgsGeometry, QgsPointXY, QgsRectangle, Qgis, \
    QgsWkbTypes, QgsCoordinateReferenceSystem, QgsCoordinateTransform, \
    QgsProject
from qgis.gui import QgsMapToolEmitPoint, QgsRubberBand


class ScriptMaptool(QgsMapToolEmitPoint):
    def __init__(self, iface, tag):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        self.tag = tag

        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rubberBand = QgsRubberBand(self.canvas,
                                        QgsWkbTypes.PolygonGeometry)
        self.rubberBand.setColor(Qt.red)
        self.rubberBand.setOpacity(0.25)
        self.rubberBand.setWidth(1)

        self.reset()

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBand.reset(QgsWkbTypes.PolygonGeometry)

    def canvasPressEvent(self, e):
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        self.showRect(self.startPoint, self.endPoint)

    def get_appname(self):
        config = configparser.ConfigParser()
        config.read(os.path.join(os.path.dirname(__file__), 'metadata.txt'))

        name = str(config.get('general', 'name'))
        return name

    def canvasReleaseEvent(self, e):
        from .functions.plotting_function import plot_data
        import time
        self.isEmittingPoint = False
        r = self.rectangle()
        if r is not None:
            figures = plot_data(self.tag, r.xMinimum(), r.yMinimum(), r.xMaximum(), r.yMaximum())
            for figure in figures:
                try:
                    hhh = figure.show()
                    # print(hhh)
                except:
                    print('Error: Could not load figures')

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())
        self.showRect(self.startPoint, self.endPoint)

    def showRect(self, startPoint, endPoint):
        self.rubberBand.reset(QgsWkbTypes.PolygonGeometry)

        if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
            return

        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(startPoint.x(), endPoint.y())
        point3 = QgsPointXY(endPoint.x(), endPoint.y())
        point4 = QgsPointXY(endPoint.x(), startPoint.y())

        self.rubberBand.addPoint(point1, False)
        self.rubberBand.addPoint(point2, False)
        self.rubberBand.addPoint(point3, False)
        self.rubberBand.addPoint(point4, True)  # true to update canvas
        self.rubberBand.show()

    def rectangle(self):
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None

        r = QgsRectangle(self.startPoint, self.endPoint)

        # Get coordinate reference system value of currently selected layer
        crs_layer = qgis.utils.iface.activeLayer().crs()

        # Transform to EPSG:25832
        crs_dest = QgsCoordinateReferenceSystem(25832)
        transform = QgsCoordinateTransform(crs_layer, crs_dest,
                                           QgsProject.instance())
        r_transformed = transform.transformBoundingBox(r)

        return r_transformed

    def deactivate(self):
        self.canvas.scene().removeItem(self.rubberBand)
        if ScriptMaptool:
            super(ScriptMaptool, self).deactivate()
            self.deactivated.emit()

